<?php

declare(strict_types = 1);

if (! function_exists('app_version')) {
    /**
     * 获取应用版本
     *
     * @return string
     * @author lyl
     */
    function app_version()
    {
        return app()->version();
    }
}

if (! function_exists('is_app_version_6')) {
    /**
     * 应用版本是否为6
     *
     * @return bool
     * @author lyl
     */
    function is_app_version_6()
    {
        $app_version         = app_version();
        $app_version_explode = explode('.', $app_version);

        if (6 == $app_version_explode[0]) {
            return true;
        } else {
            return false;
        }
    }
}

if (! function_exists('is_app_version_9')) {
    /**
     * 应用版本是否为9
     *
     * @return bool
     * @author lyl
     */
    function is_app_version_9()
    {
        $app_version         = app_version();
        $app_version_explode = explode('.', $app_version);

        if (9 == $app_version_explode[0]) {
            return true;
        } else {
            return false;
        }
    }
}

use Illuminate\Support\Str;

if (! function_exists('registerProvider')) {
    /**
     * 注册"服务提供者"到"config/app.php"
     *
     * @param string $providerName 服务商类名（带命名空间），例：App\\Providers\\PatientCommonProvider
     * @throws Exception
     * @author lyl
     */
    function registerProvider(string $providerName): void
    {
        // "配置文件app.php"的文件路径：config/app.php
        $filePath = config_path('app.php');

        // 读取"配置文件config/app.php"的内容，为一个字符串
        $fileContent = file_get_contents($filePath);

        $classString = $providerName . "::class";

        // 判断"服务提供者"是否已存在
        if (Str::contains($fileContent, $classString)) {
            return;
        }

        // 获取文件的"换行符"
        $eol = getFileEol($fileContent);

        // 唯一前置参照字符串
        $onlyBeforeContent = "App\\Providers\RouteServiceProvider::class," . $eol;

        // 追加的字符串
        $addContent = "        " . $classString . "," . $eol;

        // 文件内容追加
        fileContentAdd($filePath, $addContent, $onlyBeforeContent);
    }
}

if (! function_exists('registerAliase')) {
    /**
     * 注册"类的别名"到"config/app.php"
     *
     * @param string $aliase     类别名，例：Api
     * @param string $aliaseName 类名（带命名空间），例：\\App\\Services\\Api\\ApiService
     * @throws Exception
     * @author lyl
     */
    function registerAliase(string $aliase, string $aliaseName): void
    {
        // "配置文件app.php"的文件路径：config/app.php
        $filePath = config_path('app.php');

        // 读取"配置文件config/app.php"的内容，为一个字符串
        $fileContent = file_get_contents($filePath);

        $classString = $aliaseName . "::class";

        // 判断"服务提供者"是否已存在
        if (Str::contains($fileContent, $classString)) {
            return;
        }

        // 获取文件的"换行符"
        $eol = getFileEol($fileContent);

        // 唯一前置参照字符串
        switch (true) {
            case is_app_version_6():
                $onlyBeforeContent = "'aliases' => [" . $eol;
                break;
            case is_app_version_9():
                $onlyBeforeContent = "Facade::defaultAliases()->merge([" . $eol;
                break;
            default:
                throw new Exception('registerAliase，不支持的应用版本！');
        }

        // 追加的字符串
        $addContent = "        '" . $aliase . "' => " . $classString . "," . $eol;

        // 文件内容追加
        fileContentAdd($filePath, $addContent, $onlyBeforeContent);
    }
}

if (! function_exists('registerMiddleware')) {
    /**
     * 注册"中间件"到"app/Http/Kernel.php"
     *
     * @param string $middlewareAlias 中间件别名
     * @param string $middlewareName  中间件名（带命名空间）
     * @throws Exception
     * @author lyl
     */
    function registerMiddleware(string $middlewareAlias, string $middlewareName): void
    {
        $filePath    = app_path('Http/Kernel.php');
        $classString = $middlewareName . "::class";

        // 读取"app/Http/Kernel.php"的内容，为一个字符串
        $fileContent = file_get_contents($filePath);

        // 判断"中间件"是否已存在
        if (Str::contains($fileContent, $classString)) {
            return;
        }

        // 获取文件的"换行符"
        $eol = getFileEol($fileContent);

        $onlyBeforeContent = "\\Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified::class";
        $addContent        = "," . $eol . "        '" . $middlewareAlias . "' => " . $classString;

        // 文件内容追加
        fileContentAdd($filePath, $addContent, $onlyBeforeContent);
    }
}

if (! function_exists('getFileEol')) {
    /**
     * 获取文件换行符
     *
     * @param string $fileContent
     * @return string
     * @author lyl
     */
    function getFileEol(string $fileContent): string
    {
        // 在windows下会是“/r/n”，在linux下是“/n”，在mac下是“/r”
        // 获取文件中，各种"换行符"分别被用到的次数
        $lineEndingCount = [
            "\r\n" => mb_substr_count($fileContent, "\r\n"),
            "\r"   => mb_substr_count($fileContent, "\r"),
            "\n"   => mb_substr_count($fileContent, "\n"),
        ];

        if (! empty($lineEndingCount[PHP_EOL])) {
            return PHP_EOL;
        }

        // 给换行符用到的次数进行排序
        arsort($lineEndingCount); // 对关联数组按值进行降序排序

        // 取出被使用次数最多的"换行符"
        return array_keys($lineEndingCount)[0];
    }
}

if (! function_exists('fileContentAdd')) {
    /**
     * 文件内容追加
     *
     * @param string $filePath   文件路径
     * @param string $addContent 添加的字符串
     * @param string $search     搜索-唯一参照字符串（$searchType=0时，为直接搜索字符串；$searchType=1时，为正则字符串）
     * @param int    $position   参照字符串位置：0-字符串前置(默认) 1-字符串后置
     * @param int    $searchType 参照字符串搜索方式：0-直接搜索(默认) 1-正则搜索
     * @param bool   $onlyAdd    是否限制，添加的字符串在文件中只能出现一次：true-只添加一次；false-不限制次数
     * @throws Exception
     * @author lyl
     */
    function fileContentAdd(string $filePath, string $addContent, string $search, int $position = 0, int $searchType = 0, bool $onlyAdd = true)
    {
        // 读取文件的内容，为一个字符串
        $fileContent = file_get_contents($filePath);

        // 判断"添加的字符串"是否已存在
        if (Str::contains($fileContent, $addContent) && $onlyAdd) {
            return;
        }

        // 搜索字符串，并验证
        switch ($searchType) {
            case 0: // 直接搜索
                // "唯一前置参照字符串"在文件内容中出现的次数
                $substrCount = mb_substr_count($fileContent, $search);
                if (empty($substrCount)) {
                    throw new Exception('系统异常，文件内容追加失败，文件中找不到"唯一前置参照字符串"！' . '文件：' . $filePath . '，唯一参照字符串：' . $search);
                }
                if ($substrCount > 1) {
                    throw new Exception('系统异常，文件内容追加失败，"唯一前置参照字符串"在文件中出现不止一次！' . '文件：' . $filePath . '，唯一参照字符串：' . $search);
                }
                break;
            case 1: // 正则搜索
                // 正则匹配到的次数
                $matchCount = preg_match_all($search, $fileContent, $matches);
                if (empty($matchCount)) {
                    throw new Exception('系统异常，文件内容追加失败，文件中匹配不到"唯一前置参照字符串"！' . '文件：' . $filePath . '，唯一参照字符串搜索正则：' . $search);
                }
                if ($matchCount > 1) {
                    throw new Exception('系统异常，文件内容追加失败，"唯一前置参照字符串"在文件中匹配到不止一次！' . '文件：' . $filePath . '，唯一参照字符串搜索正则：' . $search);
                }
                $search = $matches[0][0] ?? null;
                if (is_null($search)) {
                    throw new Exception('系统异常，文件内容追加失败，未获取到匹配到的"参照字符串"！');
                }
                break;
            default:
                throw new Exception('系统异常，文件内容追加失败，不合法的"参照字符串搜索方式"！' . $searchType);
        }

        // 替换字符串
        switch ($position) {
            case 0:
                $replace = $search . $addContent;
                break;
            case 1:
                $replace = $addContent . $search;
                break;
            default:
                throw new Exception('系统异常，文件内容追加失败，不合法的"参照字符串位置"！' . $position);
        }

        // 新的配置文件内容
        $fileContentNew = str_replace($search, $replace, $fileContent);

        // 重写文件的内容
        file_put_contents($filePath, $fileContentNew);
    }
}

if (! function_exists('updatePublishFileNamespace')) {
    /**
     * 更新"发布文件"的"应用命名空间"
     *
     * @param string $laravelNamespace laravel应用命名空间，一般通过"$this->laravel->getNamespace()"获取，一般获取到的值为"App\"
     * @param string $fileAppDir       文件在app下的目录。要求：斜杠用"/"
     * @param string $className        类名。要求：类名与文件名对应，文件后缀必须为".php"
     * @author lyl
     */
    function updatePublishFileNamespace(string $laravelNamespace, string $fileAppDir, string $className)
    {
        // 获取"应用的命名空间"：APP
        $appNamespace = Str::replaceLast('\\', '', $laravelNamespace);

        // 文件在app下的目录。格式化路径，防止与期望不一致
        $fileAppDir = str_replace('\\', '/', trim($fileAppDir));

        // 类在应用下的命名空间路径。
        $classAppNamespace = str_replace('/', '\\', trim($fileAppDir));

        // 文件路径
        $filePath = app_path($fileAppDir . '/' . $className . '.php');

        // 更新"服务提供者"文件的"命名空间"
        file_put_contents(
            $filePath,
            str_replace(
                "namespace App\\" . $classAppNamespace . ";",
                "namespace {$appNamespace}\\" . $classAppNamespace . ";",
                file_get_contents($filePath)
            )
        );
    }
}
